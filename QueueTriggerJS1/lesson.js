const levenshtein = require('fast-levenshtein');

module.exports.get = function() {
    const phrases = ["What's the weather today?",
    "Are you going to go somewhere next Sunday?",
    "She sells seashells by the seashore.",
    "The pen is mightier than the sword.",
    "When in Rome, do as the Romans.",
    "After all, tomorrow is another day.",
    "What do you usually do when you are free?",
    "I work at LINE Cooporation.",
    "My favorite programming language is Perl.",
    "Where are you originally from?",
    "What computer do you have? And please don’t say a white one.",
    ];
    let index = Math.floor(Math.random() * phrases.length);
    return phrases[index];
}

module.exports.check = function(spoken, answer) {
    let dist = levenshtein.get(spoken.toLowerCase(), answer.toLowerCase());
    let max1 = levenshtein.get("", answer);
    let max2 = levenshtein.get("", spoken);

    if (max1 >= max2) {
        return ((max1 - dist) / max1) * 100; 
    } else {
        return ((max2 - dist) / max2) * 100;
    }
}