const https = require('https');
const Guid = require('guid') 
const querystring = require('querystring');
const fs = require("fs");
const cloudconvert = new (require('cloudconvert'))(process.env.CLOUD_CONVERT_KEY);
const stream = require('stream');

module.exports.recognize = function(context, audio, callback) {
    let key = process.env.SPEECH_API_SUBSCRIPTION_KEY;

    let options = {
        host: "api.cognitive.microsoft.com",
        port: 443,
        path: "/sts/v1.0/issueToken",
        method: 'POST',
        headers: {
            'Ocp-Apim-Subscription-Key': key
        }
    }
    let req = https.request(options, (res) => {
        res.setEncoding('utf8');
        res.on('data', (response) => {
            context.log('server returned ', response);
             speechToText(context, response, audio, callback);
        });
    });
    req.on('error', (e) => {
        context.log('[auth] request error: ', e.message);
    });
    req.end();
}

function speechToText(context, token, audio, callback) {
    let version= "3.0"
    let requestId = Guid.raw();
    let appId = "D4D52672-91D7-4C74-8AD8-42B1D98141A5";
    let format = "json";
    let locale = "en-US";
    let deviceOs = "Azure";
    let scenarios = "ulm";
    let instanceId = process.env.SPEECH_API_INSTANCE_ID;
    let query = querystring.stringify({
        "version": version,
        "requestid": requestId,
        "appid": appId,
        "format": format,
        "locale": locale,
        "device.os": deviceOs,
        "scenarios": scenarios,
        "instanceid": instanceId,
    });
    context.log("query is", query);
    
    let file = fs.readFileSync(__dirname + "/test.wav");

    let options = {
        host: "speech.platform.bing.com",
        port: 443,
        path: "/recognize?" + query,
        method: 'POST',
        headers: {
            'Authorization': "Bearer " + token,
            'Content-Type': 'audio/wav; codec="audio/pcm"; samplerate=8000',
            'Content-Lenth': file.byteLength
        }
    }
    let req = https.request(options, (res) => {
        res.setEncoding('utf8');
        res.on('data', (response) => {
            context.log('statusCode: ', res.statusCode)
            context.log('[cognitive] server returned ', response);
            if (res.statusCode == 200) {
                let json = JSON.parse(response);
                callback(json);
            } else {
                callback(null);
            }
        });
    });

    req.on('error', (e) => {
        context.log('[cognitive] request error: ', e.message);
        callback(null);
    });
    
    context.log("begin sending audio...");
    let audioStream = new stream.PassThrough()
    audioStream.end(audio);
    audioStream
    .pipe(cloudconvert.convert({
        "inputformat": "m4a",
        "outputformat": "wav",
        "converteroptions": {
            "audio_channels": "1",
            "audio_frequency": "8000"
        },
        "timeout": 0
    })).pipe(req).on('finish', function() {
        context.log("sending completed.");
    });
}
