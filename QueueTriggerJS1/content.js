const https = require('https');

module.exports.get = function(context, id, callback) {
    let bearer = 'Bearer ' + process.env.CHANNEL_ACCESS_TOKEN
    let options = {
        host: "api.line.me",
        port: 443,
        path: "/v2/bot/message/" + id + "/content",
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': bearer
        }
    }
    let req = https.request(options, (res) => {
        res.on('data', (response) => {
            callback(response);
        });
    });
    req.on('error', (e) => {
        context.log('[content] request error: ', e.message);
    });
    req.end();
};