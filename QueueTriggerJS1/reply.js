const https = require('https');

module.exports.texts = function(context, replyToken, texts) {
    var messages = [];
    for(var i=0; i<texts.length; i++) {
        messages.push({
                "type": "text",
                "text": texts[i]
            });
    }
    let reply = {
        "replyToken": replyToken,
        "messages": messages
    }
    let bearer = 'Bearer ' + process.env.CHANNEL_ACCESS_TOKEN
    let options = {
        host: "api.line.me",
        port: 443,
        path: "/v2/bot/message/reply",
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': bearer
        }
    }
    let req = https.request(options, (res) => {
        res.setEncoding('utf8');
        res.on('data', (response) => {
            context.log('server returned ', response);
        });
    });
    req.on('error', (e) => {
        context.log('[bot api] request error: ', e.message);
    });
    req.write(JSON.stringify(reply));
    req.end();
;}
