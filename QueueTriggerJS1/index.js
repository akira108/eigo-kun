const reply = require('./reply'); 
const content = require('./content');
const cognitive = require('./cognitive');
const lesson = require('./lesson');
const redis = require('redis');

const client = redis.createClient(6380, "akirabot.redis.cache.windows.net",
 {auth_pass: process.env.REDIS_ACCESS_KEY, tls: {servername: "akirabot.redis.cache.windows.net"}});

module.exports = function (context, myQueueItem) {
    context.log('JavaScript queue trigger function processed work item', myQueueItem);

    let event = myQueueItem;
    if (event.type === "message") {
        if (event.message.type === "text") {
            if (event.message.text === "練習") {
                client.get(event.source.userId + "/state", function(err, res) {
                    if (res === "waiting_lesson") {
                        let phrase = lesson.get();
                        // let phrase = "hello nice to meet you";
                        reply.texts(context, event.replyToken, ["始めるよ！次の英文を喋ってボイスメッセージで送ってね！", phrase]);
                        client.set(event.source.userId + "/state" , "waiting_audio", function(err, res){
                            context.log("set state", res);
                        });
                        client.set(event.source.userId + "/phrase", phrase, function(err, res) {
                            console.log("set phrase", res);
                        });
                    }
                });
            } else {
                reply.texts(context, event.replyToken, [event.message.text]);
            }
            context.done();
        } else if(event.message.type === "audio") {
            content.get(context, event.message.id, function(audio) {
                cognitive.recognize(context, audio, function(json) {
                    if (json !== null) {
                        if (json.header.status !== "success") {
                            reply.texts(context, event.replyToken, ["ごめん、もう一回試してみて！"]);    
                        } else {
                            let spoken = json.header.name;
                            client.get(event.source.userId + "/phrase", function(err, res) {
                                let score = lesson.check(spoken, res);
                                context.log("scores is", score);
                                var comment = "";
                                switch (true) {
                                    case (score < 40):
                                        comment = "難しかったかな、もう一回試してみて！";
                                        break;
                                    case (score < 60):
                                        comment = "まずまず！繰り返し練習しよう！";
                                        break;
                                    case (score < 80):
                                        comment = "かなりいい感じ！";
                                        break;
                                    case (score < 100):
                                        comment = "かなり上手！あとちょっと！";
                                        break;
                                    default:
                                        comment = "すごい！かんぺき！";
                                        break;
                                }
                                reply.texts(context, event.replyToken, ["スコアは" + score + "点！", "ネイティブにはこう聞こえるよ：\n" + spoken, comment]);
                                client.set(event.source.userId + "/state" , "waiting_lesson", function(err, res){
                                    context.log("set state", res);
                                });
                            });
                        }
                        
                    } else {
                        reply.texts(context, event.replyToken, ["ごめん、もう一回試してみて！"]);
                    }
                    context.done();
                });
            });
        }
    } else if(event.type === 'follow') {
        client.set(event.source.userId + "/state" , "waiting_lesson", function(err, res){
            reply.texts(context, event.replyToken, ["お友達追加ありがとう", "さっそく発音の練習をしよう！", "「練習」と言ってくれれば始まるよ！"]);
            context.log("set state", res);
            context.done();
        });
    }

};
