module.exports = function (context, req) {
    context.log('JavaScript HTTP trigger function processed a request.');
    context.log(req.body);

    if (req.body) {
        res = {
            // status: 200, /* Defaults to 200 */
            body: "OK"
        };
        context.bindings.messageQueueItem = req.body.events
         
    } else {
        res = {
            status: 400,
            body: "body is empty"
        };
    }
    context.done(null, res);
};
